import React from 'react';
import style from "./Dialogs.module.css"
import DialogItem from './DialogItem/DialogsItem.js'
import MessageItem from './MessageItem/MessageItem.js'

class Dialogs extends React.Component {
	render() {
		let dialogsData = [
			{id: 'Di', name: "Dimych"},
			{id: 'And', name: "Andrey"},
			{id: 'Sv', name: "Sveta"},
			{id: 'Sas', name: "Sasha"},
			{id: 'Vic', name: "Victro"},
			{id: 'Val', name: "Valeriy"}
		];

		let messagesData = [
			{id: 1, message: "Hi"},
			{id: 2, message: "Kamasutra"},
			{id: 3, message: "Yo"},
			{id: 4, message: "Yo"},
			{id: 5, message: "Yo"},
			{id: 6, message: "Yo"}
		];

		let dialogsElements = dialogsData.map(d => <DialogItem name={d.name} id={d.id}/>);
		let messagesElements = messagesData.map(m => <MessageItem message={m.message}/>);

		return (
			<div className={style.dialogs}>
	<div className={style.dialogsItems}>
		{dialogsElements}
	</div>

		<div className={style.messages}>
		{messagesElements}
	</div>
		</div>
	)
	}
}
export default Dialogs;