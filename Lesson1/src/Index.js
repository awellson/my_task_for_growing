import React from 'react';
import './App.css';
import {BrowserRouter, Route} from "react-router-dom"
//Components
import Header from './components/Header/Header.js';
import Navbar from './components/Navbar/Navbar.js';
import Profile from './components/Profile/Profile.js';
import Dialogs from './components/Dialogs/Dialogs.js';

//Lessons
import Dialogs from "./components/GrowingTask/LessonOne/Dialogs.js"

const App = (props) => {
	let dialogsComponent = () => <Dialogs/>;


	return (
		<BrowserRouter>
			<div className="wrapper">
				<div className="wrapper-content">
					<Route path="/dialogs" render={ dialogsComponent }/>
				</div>
			</div>
		</BrowserRouter>
	);
};

export default App;
