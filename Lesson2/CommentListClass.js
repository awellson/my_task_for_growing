import React from 'react';
import style from "./../Dialogs.module.css"
import { NavLink } from "react-router-dom"

class DialogItem extends React.Component {

	render() {
		let path = "/dialogs/" + props.id;

		return (
			<div className={style.dialog}>
				<NavLink to={ path } activeClassName={style.active}>
					{ props.name }
				</NavLink>
			</div>
		)
	}
}
export default DialogItem;