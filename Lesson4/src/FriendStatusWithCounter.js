//Один из ключевых моментов, что в отличии от хуков, классовые методы жизненного цикла
// часто содержат логику, которая никак между собой не связана, в то время как связанная логика, разбивается на несколько методо
class FriendStatusWithCounter extends React.Component {
	constructor(props) {
		super(props);
		this.state = {count: 0, isOnline: null};
		this.handleStatusChange = this.handleStatusChange.bind(this);
	}

	componentDidMount() {
		document.title = `Вы нажали ${this.state.count} раз`;
		ChatAPI.subscribeToFriendStatus(
			this.props.friend.id,
			this.handleStatusChange
		);
	}

	componentDidUpdate() {
		document.title = `Вы нажали ${this.state.count} раз`;
	}

	componentWillUnmount() {
		ChatAPI.unsubscribeFromFriendStatus(
			this.props.friend.id,
			this.handleStatusChange
		);
	}

	handleStatusChange(status) {
		this.setState({
			isOnline: status.isOnline
		});
	}
}
//Можно использовать хук состояния более одного раза

function FriendStatusWithCounter(props) {
	const [count, setCount] = useState(0);//1
	useEffect(() => {
		document.title = `Вы нажали ${count} раз`;
	});

	const [isOnline, setIsOnline] = useState(null);//2
	useEffect(() => {
		function handleStatusChange(status) {
			setIsOnline(status.isOnline);
		}

		ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
		return () => {
			ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
		};
	});
}