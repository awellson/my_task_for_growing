import React, { useState, useEffect } from 'react';

//Class component
export class Example extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			count: 0
		};
	}

	componentDidMount() {
		document.title = `Вы нажали ${this.state.count} раз`;
	}
//нам приходится дублировать наш код между этими классовыми методами жизненного цикла.
	componentDidUpdate() {
		document.title = `Вы нажали ${this.state.count} раз`;
	}

	render() {
		return (
			<div>
				<p>Вы нажали {this.state.count} раз</p>
				<button onClick={() => this.setState({ count: this.state.count + 1 })}>
					Нажми на меня
				</button>
			</div>
		);
	}
}

//Тоже самое только с Хуками

//UseEffect
export function ExampleTwo() {
	const [count, setCount] = useState(0);

	// По принципу componentDidMount и componentDidUpdate:
	useEffect(() => {
		console.log("====>> useEffect", useEffect)
		// Обновляем заголовок документа, используя API браузера
		document.title = `Вы нажали ${count} раз`;
	});

	return (
		<div>
			<p>Вы нажали {count} раз</p>
			<button onClick={() => setCount(count + 1)}>
				Нажми на меня
			</button>
		</div>
	);
}
