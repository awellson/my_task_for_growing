import React, { useState, useEffect } from 'react';

function FriendStatus(props) {
	const [isOnline, setIsOnline] = useState(null);

	useEffect(() => {
		function handleStatusChange(status) {
			setIsOnline(status.isOnline);
		}

		ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
		// Указываем, как сбросить этот эффект:
		return function cleanup() {
			API.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
		};
	});

	if (isOnline === null) {
		return 'Загрузка...';
	}
	return isOnline ? 'В сети' : 'Не в сети';
}