import React from 'react';
import './App.css';
import {BrowserRouter, Route} from "react-router-dom"
//Lessons4
import { Example, ExampleTwo }  from "./components/GrowingTask/LessonFour/Example.js"


const App = (props) => {
	return (
		<BrowserRouter>
			<div className="wrapper">

				<Example/>
				<ExampleTwo/>

			</div>
		</BrowserRouter>
	);
};

export default App;
