import React from 'react';

class LifeCicle extends React.Component {
	constructor(props) {
		super(props);
		this.state = {class: "default", label: "class default", design: "#ff5959"}

		this.onClick = this.onClick.bind(this);

		console.log("constructor");
	}
	UNSAFE_componentWillReceiveProps(nextProps) {
		console.log("UNSAFE_componentWillReceiveProps()");
	}
	UNSAFE_componentWillMount(){
		console.log("UNSAFE_componentWillMount()");
	}
	componentDidMount(){
		console.log("componentDidMount()");
	}
	shouldComponentUpdate(){
		console.log("shouldComponentUpdate()");
		return true;
	}
	UNSAFE_componentWillUpdate(){
		console.log("UNSAFE_componentWillUpdate()");
	}
	componentDidUpdate(){
		console.log("componentDidUpdate()");
	}
	componentWillUnmount(){
		console.log("componentWillUnmount()");
	}
	onClick(){
		var className = ( this.state.class === "default" ) ? "active" : "default";
		this.setState({class: className});

		var style = ( this.state.design === "#ff5959" ) ? "#48bf68" : "#ff5959";
		this.setState({design: style});

		var label = ( this.state.label === "class default" ) ? "class active" : "class default";
		this.setState({label: label});
	}

	render() {
		console.log("render()");
		return <button style={{ background: this.state.design }} onClick={this.onClick} className={this.state.class}>{this.state.label}</button>;
	}
}
export default LifeCicle