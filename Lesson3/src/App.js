import React from 'react';
import './App.css';
import {BrowserRouter, Route} from "react-router-dom"

//Lessons3
import ErrorBoundary from "./components/GrowingTask/LessonTwo/DidCatch.js"
import { Example, ExampleTwo }  from "./components/GrowingTask/LessonFour/Example.js"

const App = (props) => {

	class BuggyCounter extends React.Component {
		constructor(props) {
			super(props);
			this.state = { counter: 0 };
			this.handleClick = this.handleClick.bind(this);
		}

		handleClick() {
			this.setState(({counter}) => ({
				counter: counter + 1
			}));
		}

		render() {
			if (this.state.counter === 5) {
				// Simulate a JS error
				throw new Error('I crashed!');
			}
			return <h1 onClick={this.handleClick}>{this.state.counter}</h1>;
		}
	}

	return (
		<BrowserRouter>
			<div className="wrapper">
				<div>
					<p>
						<b>
							This is an example of error boundaries.
							<br/><br/>
							Click on the numbers to increase the counters.
							<br/>
							The counter is programmed to throw when it reaches 5. This simulates a JavaScript error in a component.
						</b>
					</p>
					<hr/>
					<ErrorBoundary>
						<p>These two counters are inside the same error boundary. If one crashes, the error boundary will replace
							both of them.</p>
						<BuggyCounter/>
						<BuggyCounter/>
					</ErrorBoundary>
					<hr/>
					<p>These two counters are each inside of their own error boundary. So if one crashes, the other is not
						affected.</p>
					<ErrorBoundary><BuggyCounter/></ErrorBoundary>
					<ErrorBoundary><BuggyCounter/></ErrorBoundary>
				</div>

				<div className="wrapper-content">
					<Route path="/dialogs" render={ profileComponent }/>
					<Route path="/profile" render={ dialogsComponent }/>
				</div>


			</div>
		</BrowserRouter>
	);
};

export default App;
